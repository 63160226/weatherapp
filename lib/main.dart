import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false,
      home: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "Chonburi",

            ),
          ),
          body: Center(
            child: Stack(
              children: <Widget>[
                Container(
                  decoration:BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage("https://image.bangkokbiznews.com/uploads/images/md/2021/11/HxtaBuHuTOvxfbWB1DYB.webp"),
                        fit: BoxFit.cover,
                    )
                  ) ,
                ),


                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                   Padding(padding: const EdgeInsets.all(50.0),
                   child: Icon(
                     Icons.cloudy_snowing,
                     size: 200,
                     color: Color.fromARGB(255, 51, 75, 111),

                   ),
                   ),
                    _temp(),
                    _yesterday(),
                    _today(),
                    _tomorrow(),

                  ],
                ),
                Column(

                )
              ],
            ),
          )


      ),



    );

  }
}

_tomorrow() {
  return Row(
    children: [
      Icon(Icons.sunny,color: Color.fromARGB(255, 255, 251, 0),)
      ,
      SizedBox(
        width: 10,


      ),

      Text("Tomorrow : 29 ํ/16 ํ ",style: TextStyle(fontSize: 20,
          color: Colors.white
      ),
      ),

    ],
  );
}

_today() {
  return Row(
    children: [
      Icon(Icons.sunny,color: Color.fromARGB(255, 255, 251, 0),)
      ,
      SizedBox(
        width: 10,


      ),

      Text("Today : 30 ํ/16 ํ ",style: TextStyle(fontSize: 20,
          color: Colors.white
      ),
      ),

    ],
  );
}
_yesterday(){
  return Row(
    children: [
      Icon(Icons.nightlight,color: Color.fromARGB(255, 255, 251, 0),)
      ,
      SizedBox(
        width: 10,


      ),

      Text("ํYesterday : 31 ํ/17 ํ ",style: TextStyle(fontSize: 20,
          color: Colors.white
      ),
      ),

    ],
  );
}
_temp(){
  return Text("23 ํc",style: TextStyle(fontSize: 80,
      fontWeight: FontWeight.w400,
        color: Colors.white

  ),
textAlign: TextAlign.center,
  );
}
